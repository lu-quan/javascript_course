'use strict'
/*
1、题目描述
找出元素 item 在给定数组 arr 中的位置
输出描述:
如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
示例1
输入
[ 1, 2, 3, 4 ], 3
输出
2

function indexOf(arr, item) {

}
*/


/*
2、题目描述
计算给定数组 arr 中所有元素的总和
输入描述:
数组中的元素均为 Number 类型
示例1
输入

[ 1, 2, 3, 4 ]
输出

10

function sum(arr) {

}
*/


/*
3、题目描述
移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4, 2], 2
输出

[1, 3, 4]

function remove(arr, item) {

}
*/


/*
4、题目描述
移除数组 arr 中的所有值与 item 相等的元素，直接在给定的 arr 数组上进行操作，并将结果返回
示例1
输入

[1, 2, 2, 3, 4, 2, 2], 2
输出

[1, 3, 4]

function removeWithoutCopy(arr, item) {

}
*/

/*
5、题目描述
在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4],  10
输出

[1, 2, 3, 4, 10]

function append(arr, item) {
    
}
*/

/*
6、题目描述
删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 2, 3]

function tcate(arr) {

}
*/


/*
7、题目描述
在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], 10
输出

[10, 1, 2, 3, 4]

function prepend(arr, item) {

}
*/

/*
8、题目描述
删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[2, 3, 4]

function curtail(arr) {

}
*/


/*
9、题目描述
合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], ['a', 'b', 'c', 1]
输出

[1, 2, 3, 4, 'a', 'b', 'c', 1]

function concat(arr1, arr2) {

}
*/

/*
10、题目描述
在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], 'z', 2
输出

[1, 2, 'z', 3, 4]

function insert(arr, item, index) {

}
*/


/*
11、题目描述
统计数组 arr 中值等于 item 的元素出现的次数
示例1
输入

[1, 2, 4, 4, 3, 4, 3], 4
输出

3

function count(arr, item) {

}
*/


/*
12、题目描述
找出数组 arr 中重复出现过的元素
示例1
输入

[1, 2, 4, 4, 3, 3, 1, 5, 3]
输出

[1, 3, 4]

function duplicates(arr) {

}
*/

/*
13、题目描述
为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 4, 9, 16]

function square(arr) {

}
*/


/*
14、题目描述
在数组 arr 中，查找值与 item 相等的元素出现的所有位置
示例1
输入

['a','b','c','d','e','f','a','b','c'] 'a'
输出

[0, 6]

function findAllOccurrences(arr, target) {

}
*/

/*
15、题目描述
给定的 js 代码中存在全局变量，请修复

function globals() {
    myObject = {
      name : 'Jory'
    };

    return myObject;
}
*/

/*
16、题目描述
请修复给定的 js 代码中，函数定义存在的问题
示例1
输入

true
输出

a

function functions(flag) {
    if (flag) {
      function getValue() { return 'a'; }
    } else {
      function getValue() { return 'b'; }
    }

    return getValue();
}
*/


/*
17、题目描述
修改 js 代码中 parseInt 的调用方式，使之通过全部测试用例
示例1
输入

'12'
输出

12
示例2
输入

'12px'
输出

12
示例3
输入

'0x12'
输出

0

function parse2Int(num) {
    return parseInt(num);
}
*/

/*
18、题目描述
判断 val1 和 val2 是否完全等同

function identity(val1, val2) {

}
*/

/*
19、题目描述
实现一个打点计时器，要求
1、从 start 到 end（包含 start 和 end），每隔 100 毫秒 console.log 一个数字，每次数字增幅为 1
2、返回的对象中需要包含一个 cancel 方法，用于停止定时操作
3、第一个数需要立即输出

function count(start, end) {

}
*/

/*
20、题目描述
实现 fizzBuzz 函数，参数 num 与返回值的关系如下：
1、如果 num 能同时被 3 和 5 整除，返回字符串 fizzbuzz
2、如果 num 能被 3 整除，返回字符串 fizz
3、如果 num 能被 5 整除，返回字符串 buzz
4、如果参数为空或者不是 Number 类型，返回 false
5、其余情况，返回参数 num
示例1
输入

15
输出

fizzbuzz

function fizzBuzz(num) {

}
*/


/*
21、题目描述
将数组 arr 中的元素作为调用函数 fn 的参数
示例1
输入

function (greeting, name, punctuation) {return greeting + ', ' + name + (punctuation || '!');}, ['Hello', 'Ellie', '!']
输出

Hello, Ellie!

function argsAsArray(fn, arr) {

}
*/

/*
22、题目描述
将函数 fn 的执行上下文改为 obj 对象
示例1
输入

function () {return this.greeting + ', ' + this.name + '!!!';}, {greeting: 'Hello', name: 'Rebecca'}
输出

Hello, Rebecca!!!


function speak(fn, obj) {

}
*/

/*
23、题目描述
实现函数 functionFunction，调用之后满足如下条件：
1、返回值为一个函数 f
2、调用返回的函数 f，返回值为按照调用顺序的参数拼接，拼接字符为英文逗号加一个空格，即 ', '
3、所有函数的参数数量为 1，且均为 String 类型
示例1
输入

functionFunction('Hello')('world')
输出

Hello, world

function functionFunction(str) {

}
*/

/*
24、题目描述
实现函数 makeClosures，调用之后满足如下条件：
1、返回一个函数数组 result，长度与 arr 相同
2、运行 result 中第 i 个函数，即 result[i]()，结果与 fn(arr[i]) 相同
示例1
输入

[1, 2, 3], function (x) { 
	return x * x; 
}
输出

4

function makeClosures(arr, fn) {

}
*/

/*
25、题目描述
已知函数 fn 执行需要 3 个参数。请实现函数 partial，调用之后满足如下条件：
1、返回一个函数 result，该函数接受一个参数
2、执行 result(str3) ，返回的结果与 fn(str1, str2, str3) 一致
示例1
输入

var sayIt = function(greeting, name, punctuation) {     return greeting + ', ' + name + (punctuation || '!'); };  partial(sayIt, 'Hello', 'Ellie')('!!!');
输出

Hello, Ellie!!!

function partial(fn, str1, str2) {

}
*/

/*
26、题目描述
函数 useArguments 可以接收 1 个及以上的参数。请实现函数 useArguments，返回所有调用参数相加后的结果。本题的测试参数全部为 Number 类型，不需考虑参数转换。
示例1
输入

1, 2, 3, 4
输出

10

function useArguments() {

}
*/

/*
27、题目描述
实现函数 callIt，调用之后满足如下条件
1、返回的结果为调用 fn 之后的结果
2、fn 的调用参数为 callIt 的第一个参数之后的全部参数
示例1
输入

无
输出

无

function callIt(fn) {
    
}
*/

/*
28、题目描述
实现函数 partialUsingArguments，调用之后满足如下条件：
1、返回一个函数 result
2、调用 result 之后，返回的结果与调用函数 fn 的结果一致
3、fn 的调用参数为 partialUsingArguments 的第一个参数之后的全部参数以及 result 的调用参数
示例1
输入

无
输出

无

function partialUsingArguments(fn) {

}
*/

/*
29、题目描述
已知 fn 为一个预定义函数，实现函数 curryIt，调用之后满足如下条件：
1、返回一个函数 a，a 的 length 属性值为 1（即显式声明 a 接收一个参数）
2、调用 a 之后，返回一个函数 b, b 的 length 属性值为 1
3、调用 b 之后，返回一个函数 c, c 的 length 属性值为 1
4、调用 c 之后，返回的结果与调用 fn 的返回值一致
5、fn 的参数依次为函数 a, b, c 的调用参数
示例1
输入

var fn = function (a, b, c) {return a + b + c}; curryIt(fn)(1)(2)(3);
输出

6

function curryIt(fn) {

}
*/

/*
30、题目描述
返回参数 a 和 b 的逻辑或运算结果
示例1
输入

false, true
输出

true

function or(a, b) {

}
*/

/*
31、题目描述
返回参数 a 和 b 的逻辑且运算结果
示例1
输入

false, true
输出

false

function and(a, b) {

}
*/

/*
32、题目描述
完成函数 createModule，调用之后满足如下要求：
1、返回一个对象
2、对象的 greeting 属性值等于 str1， name 属性值等于 str2
3、对象存在一个 sayIt 方法，该方法返回的字符串为 greeting属性值 + ', ' + name属性值

function createModule(str1, str2) {

}
*/

/*
33、题目描述
获取数字 num 二进制形式第 bit 位的值。注意：
1、bit 从 1 开始
2、返回 0 或 1
3、举例：2 的二进制为 10，第 1 位为 0，第 2 位为 1
示例1
输入

128, 8
输出

1

function valueAtBit(num, bit) {

}
*/

/*
34、题目描述
给定二进制字符串，将其换算成对应的十进制数字
示例1
输入

'11000000'
输出

192

function base10(str) {

}
*/

/*
35、题目描述
将给定数字转换成二进制字符串。如果字符串长度不足 8 位，则在前面补 0 到满8位。
示例1
输入

65
输出

01000001

function convertToBinary(num) {

}
*/

/*
36、题目描述
求 a 和 b 相乘的值，a 和 b 可能是小数，需要注意结果的精度问题
示例1
输入

3, 0.0001
输出

0.0003

function multiply(a, b) {

}
*/

/*
37、题目描述
将函数 fn 的执行上下文改为 obj，返回 fn 执行后的值
示例1
输入

alterContext(function() {return this.greeting + ', ' + this.name + '!'; }, {name: 'Rebecca', greeting: 'Yo' })
输出

Yo, Rebecca!

function alterContext(fn, obj) {

}
*/

/*
38、题目描述
给定一个构造函数 constructor，请完成 alterObjects 方法，将 constructor 的所有实例的 greeting 属性指向给定的 greeting 变量。
示例1
输入

var C = function(name) {this.name = name; return this;}; 
var obj1 = new C('Rebecca'); 
alterObjects(C, 'What\'s up'); obj1.greeting;
输出

What's up

function alterObjects(constructor, greeting) {

}
*/

/*
39、题目描述
找出对象 obj 不在原型链上的属性(注意这题测试例子的冒号后面也有一个空格~)
1、返回数组，格式为 key: value
2、结果数组不要求顺序
示例1
输入

var C = function() {this.foo = 'bar'; this.baz = 'bim';}; 
C.prototype.bop = 'bip'; 
iterate(new C());
输出

["foo: bar", "baz: bim"]

function iterate(obj) {

}
*/

/*
40、题目描述
给定字符串 str，检查其是否包含数字，包含返回 true，否则返回 false
示例1
输入

'abc123'
输出

true

function containsNumber(str) {

}
*/

/*
41、题目描述
给定字符串 str，检查其是否包含连续重复的字母（a-zA-Z），包含返回 true，否则返回 false
示例1
输入

'rattler'
输出

true

function containsRepeatingLetter(str) {

}
*/

/*
42、题目描述
给定字符串 str，检查其是否以元音字母结尾
1、元音字母包括 a，e，i，o，u，以及对应的大写
2、包含返回 true，否则返回 false
示例1
输入

'gorilla'
输出

true

function endsWithVowel(str) {

}
*/

/*
43、题目描述
给定字符串 str，检查其是否包含 连续3个数字
1、如果包含，返回最新出现的 3 个数字的字符串
2、如果不包含，返回 false
示例1
输入

'9876543'
输出

987

function captureThreeNumbers(str) {

}
*/

/*
44、题目描述
给定字符串 str，检查其是否符合如下格式
1、XXX-XXX-XXXX
2、其中 X 为 Number 类型
示例1
输入

'800-555-1212'
输出

true

function matchesPattern(str) {

}
*/

/*
45、题目描述
给定字符串 str，检查其是否符合美元书写格式
1、以 $ 开始
2、整数部分，从个位起，满 3 个数字用 , 分隔
3、如果为小数，则小数部分长度为 2
4、正确的格式如：$1,023,032.03 或者 $2.03，错误的格式如：$3,432,12.12 或者 $34,344.3
示例1
输入

'$20,933,209.93'
输出

true

function isUSD(str) {

}
*/
